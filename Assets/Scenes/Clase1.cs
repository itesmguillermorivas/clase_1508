﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clase1 : MonoBehaviour {


    // exponiendo variables al público
    public float speed;
    public float ejemplito = 4;
    public int score = 100;
    public GameObject objeto;

    // exponiendo variables privadas
    [SerializeField]
    private string noMeMuevas = "Hola";

    // ciclo de vida
    // - nuestros scripts se acoplan a una logica centralizada que ya tiene un orden

    // se ejecuta al iniciar el objeto una sola vez
    void Awake () {
        print("AWAKE " + transform.name);

    }

    // se ejecuta una vez después de todos los awakes
	void Start () {
        Debug.Log("START");
        print(objeto.transform.name);

        Text textito = objeto.GetComponent<Text>();
        // NOTA: ESTO PUEDE REGRESAR NULL
        textito.text = "hola amiguitos";

	}
	
	// Update is called once per frame
	void Update () {
        // fps - frames per second
        // framerate - 30 fps
        // aplicación en tiempo real
        // corre tantas veces pueda

        // 2 cosas que queremos poner aqui
        // - detección de entrada de usuario
        // - movimiento
        // print("UPDATE");


        // input
        // 2 maneras 
        // detectando directo de los dispositivos
        // TRUE - si en el cuadro anterior estaba suelta
        // y en el cuadro actual está presionada
        if (Input.GetKeyDown(KeyCode.A)) {
            print("KEY DOWN");
        }

        // TRUE - cuadro anterior presionado
        // cuadro actual presionado
        if (Input.GetKey(KeyCode.A)) {
            print("KEY");
        }

        // TRUE - cuadro anterior presionado
        // cuadro actual suelto
        if (Input.GetKeyUp(KeyCode.A)) {
            print("KEY UP");
        }

        // mouse 
        if (Input.GetMouseButtonDown(0)) {
            print(Input.mousePosition);
        }

        // ejes (esta es la chida)
        // regresan un float
        // float tiene valor [-1, 1]
        // float h = Input.GetAxisRaw("Horizontal");
        float h = Input.GetAxis("Horizontal");
        //print(h);
        float v = Input.GetAxis("Vertical");


        transform.Translate(
            h * Time.deltaTime * speed, 
            v * Time.deltaTime * speed, 
            0,
            Space.World
            );
    }

    // corre una vez al frame
    // después del update
    void LateUpdate()
    {
        // print("LATE UPDATE");
    }

    // fixed - fijo 
    // corre en una frecuencia preconfigurada
    void FixedUpdate() {
        // print("FIXED UPDATE");
    }

    // REGLAS PARA COLISIONES
    // 2 objetos o más
    // todos los involucrados tienen algún tipo de collider
    // uno necesita tener un componente que se llama rigidbody
    // el que tiene rigidbody se debe mover

    // 3 momentos en la vida de la colisión
    // estos eventos se detonan en TODOS los involucrados
    void OnCollisionEnter(Collision c){
        print("ENTER " + c.transform.name);
        score--;
    }

    void OnCollisionStay(Collision c) {
        //print("STAY");
    }

    void OnCollisionExit(Collision c) {
        //print("EXIT");
    }

    void OnTriggerEnter(Collider c) {

        print("SI FUNCIONA! " + c.transform.name);
    }

    void OnTriggerStay(Collider c) { }

    void OnTriggerExit(Collider c) { }
}
