﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladrillo : MonoBehaviour {

    private Rigidbody rb;


	// Use this for initialization
	void Start () {
        // Destroy 
        // destruir 
        // 2 cosas: game objects, componentes
        // Destroy(gameObject, 3);
        rb = GetComponent<Rigidbody>();
        //Destroy(rb, 0.5f);
        rb.AddExplosionForce(1000, Vector3.zero, 10);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision c) {

        if (c.transform.name == "Proyectil(Clone)") {
            //print("PROYECTIL ME PEGO");
        }

        if (c.gameObject.layer == 10) {
            print("LAYER 10");
            Destroy(gameObject);
        }

        if (c.transform.tag == "Proyectil") {
            //print("TAG PROYECTIL");
        }
    }
}
