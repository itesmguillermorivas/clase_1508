﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canion : MonoBehaviour {

    // no hay manera de generar un nuevo game object sin referencia
    // lo que hacemos es clonar

    public GameObject original;
    public Transform referencia;

    private IEnumerator c1;
    private Coroutine c2;

	// Use this for initialization
	void Start () {
        c1 = Ejemplo();
        c2 = StartCoroutine(c1);
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.Space)) {

            // el código de la clonación
            // Cuaternión - ?!?!?!?!?!
            // vector de 4 valores que se usa para describir
            // la rotación de un objeto en un espacio tridimensional
            Instantiate(original, referencia.position, original.transform.rotation);
            // detiene todas las corrutinas del script
            //StopAllCoroutines();
            StopCoroutine(c1);
        }

        float h = Input.GetAxis("Horizontal");
        transform.Translate(h * Time.deltaTime * 5, 0, 0);
	}

    // corutina
    // - mecanismo para simular hilos (threads) (concurrencia)
    // - pseudohilo
    // - update van 2 cosas: 
    // -- movimiento
    // -- entrada
    // hay comportamiento recurrente que no queremos en el update
    IEnumerator Ejemplo() {
        while (true) { 
            yield return new WaitForSeconds(0.5f);
            print("SI LLEGO");
        }
    }
}
